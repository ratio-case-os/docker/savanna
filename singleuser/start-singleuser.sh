#!/bin/bash
set -e
# set default ip to 0.0.0.0
if [[ "${NOTEBOOK_ARGS} $*" != *"--ip="* ]]; then
    NOTEBOOK_ARGS="--ip=0.0.0.0 ${NOTEBOOK_ARGS}"
fi
jupyterhub-singleuser ${NOTEBOOK_ARGS} "$@"
