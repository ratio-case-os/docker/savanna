ARG FROM
FROM ${FROM}

ENV PYTHONDONTWRITEBYTECODE=1 \
    NB_USER=${USER} \
    NB_UID=${UID} \
    NB_GID=${GID} \
    JUPYTER_ENABLE_LAB=1 \
    JPY_USER=${USER} \
    NOTEBOOK_DIR=${HOME}/work

# Install Python deps.
USER root
RUN --mount=type=bind,source=/pip.txt,target=/pip.txt \
    pip install -r /pip.txt \
    && pip install --no-deps "automathon>=0.0.12" \
    && raesl jupyter install \
    && jupyter labextension disable "@jupyterlab/apputils-extension:announcements" \
    && chown -R ${UID}:${GID} /home/${USER}
RUN curl -fsSL https://code-server.dev/install.sh | sh \
    && rm -rf /home/**/.cache/

USER ${USER}
COPY --chown=${UID}:${GID} /singleuser/config.yaml ${HOME}/.config/code-server/config.yaml
COPY --chown=${UID}:${GID} /singleuser/settings.json ${HOME}/.config/code-server/Machine/settings.json
RUN export TMP=${HOME}/.tmp \
    && code-server --user-data-dir=${TMP} --install-extension ms-python.python \
    && code-server --user-data-dir=${TMP} --install-extension ratio-inno.elephant-specification-language \
    && code-server --user-data-dir=${TMP} --install-extension PKief.material-icon-theme \
    && code-server --user-data-dir=${TMP} --install-extension streetsidesoftware.code-spell-checker \
    && code-server --user-data-dir=${TMP} --install-extension streetsidesoftware.code-spell-checker-dutch \
    && code-server --user-data-dir=${TMP} --install-extension usernamehw.errorlens \
    && code-server --user-data-dir=${TMP} --install-extension tamasfe.even-better-toml \
    && code-server --user-data-dir=${TMP} --install-extension mhutchie.git-graph \
    && code-server --user-data-dir=${TMP} --install-extension GitLab.gitlab-workflow \
    && code-server --user-data-dir=${TMP} --install-extension ms-python.black-formatter \
    && code-server --user-data-dir=${TMP} --install-extension ms-python.isort \
    && code-server --user-data-dir=${TMP} --install-extension ms-python.mypy-type-checker \
    && code-server --user-data-dir=${TMP} --install-extension ms-toolsai.jupyter \
    && code-server --user-data-dir=${TMP} --install-extension skellock.just \
    && code-server --user-data-dir=${TMP} --install-extension James-Yu.latex-workshop \
    && code-server --user-data-dir=${TMP} --install-extension mechatroner.rainbow-csv \
    && code-server --user-data-dir=${TMP} --install-extension sndst00m.vscode-native-svg-preview \
    && code-server --user-data-dir=${TMP} --install-extension redhat.vscode-xml \
    && code-server --user-data-dir=${TMP} --install-extension redhat.vscode-yaml \
    && code-server --user-data-dir=${TMP} --install-extension PascalReitermann93.vscode-yaml-sort \
    && code-server --user-data-dir=${TMP} --install-extension richie5um2.vscode-sort-json \
    && code-server --user-data-dir=${TMP} --install-extension esbenp.prettier-vscode \
    && rm -r ${TMP}

# Add config and startup scripts and allow execution.
COPY --chown=${UID}:${GID} /singleuser/jupyter_server_config.py /etc/jupyter/
COPY --chown=${UID}:${GID} \
    /singleuser/start-notebook.sh \
    /singleuser/start-singleuser.sh \
    /usr/local/bin/
COPY --chown=${UID}:${GID} /singleuser/config.yaml ${HOME}/.config/code-server/config.yaml
COPY --chown=${UID}:${GID} /singleuser/settings.json ${HOME}/.config/code-server/Machine/settings.json
COPY --chown=${UID}:${GID} /singleuser/vscode.svg ${HOME}/.config/code-server/vscode.svg
RUN chmod a+rx \
    /usr/local/bin/start-notebook.sh \
    /usr/local/bin/start-singleuser.sh

WORKDIR ${HOME}/work
ENTRYPOINT ["tini", "-g", "--"]
CMD ["start-notebook.sh"]
EXPOSE 8888
ENV PYTHONDONTWRITEBYTECODE=
